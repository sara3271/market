import HomePage from './HomePage';
import ShopList from './ShopList';
import SearchPage from './SearchPage';
import Categories from './Categories';
import CartPage from './CartPage';

export { HomePage, ShopList, SearchPage, Categories, CartPage };
