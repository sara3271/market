import React, { Component } from 'react';
import {
  View,
  Image,
  TouchableOpacity,
  Text,
  TextInput,
  Dimensions,
  StyleSheet,
  FlatList,
  StatusBar,
  ScrollView,
} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialIcons';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as R from 'ramda';

import Colors from '../constants/colors';
import { getItemsQuery } from '@actions';
import constants from '../redux/interface/constants';
import {
  BigBanner,
  HorizontalCategory,
  VerticalCategory,
  Header,
} from '@components';

class HomePage extends Component {
  async componentDidMount() {
    await this.props.getItems();
  }

  render() {
    return (
      <View style={{ flex: 1, backgroundColor: Colors.mainBackgroundColor }}>
        <StatusBar
          backgroundColor={Colors.headerColor}
          barStyle="dark-content"
        />
        <Header navigation={this.props.navigation} />
        <ScrollView>
          {this.props.items.map(item => {
            switch (item.type) {
              case constants.CAMPAIGN_BANNER:
                return <BigBanner banners={item.banners} />;
              case constants.CATEGORY_LIST_VERTICAL:
                return <VerticalCategory products={item.banners} />;

              case constants.TIMER_HORIZONTAL:
                return (
                  <HorizontalCategory
                    products={item.products}
                    images={item.image}
                  />
                );
            }
          })}
        </ScrollView>
      </View>
    );
  }
}

const mapStateToProps = state => {
  return {
    items: state.items,
  };
};

const mapActionsToProps = dispatch => {
  return bindActionCreators(
    {
      getItems: getItemsQuery,
    },
    dispatch,
  );
};

export default connect(mapStateToProps, mapActionsToProps)(HomePage);
