export default {
  mainBackgroundColor: '#F7F7F7',
  headerColor: '#D1E5F8',
  tabIconColor: '#2272E6',
  topTextColor: '#6f6f6f',
  bottomTextColor: '#767676',
  searchInputBorder: '#AFD4F6',
  white: 'white',
  gray: 'gray',
  lightGreen: '#52A22F',
  drawerBackgroundColor: '#338fc0',
};
