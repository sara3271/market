import { GET_ITEMS } from './actionTypes';
import { getProductsQuery } from '../../network/queries';
import { itemInterface } from '@interface';
import constants from '@interface/constants';

export function getItems(items) {
  return {
    type: GET_ITEMS,
    payload: {
      items,
    },
  };
}

export const getItemsQuery = () => {
  return async dispatch => {
    const items = await getProductsQuery();
    let convertedItems = [];
    for (let itemsObject of items) {
      convertedItems.unshift(itemInterface(itemsObject));
    }
    convertedItems = convertedItems.filter(item =>
      Object.keys(constants)
        .map(key => constants[key])
        .includes(item.type),
    );
    //console.warn('salam', convertedItems);
    dispatch(getItems(convertedItems));
  };
};
