import thunk from 'redux-thunk';
import { createStore, applyMiddleware, compose } from 'redux';
import { persistStore, persistReducer } from 'redux-persist';
import storage from 'redux-persist/lib/storage';

import reducers from '@reducers';

const storeEnhancer = compose(applyMiddleware(thunk));

const persistConfig = {
  key: 'root',
  storage,
};

const persistedReducer = persistReducer(persistConfig, reducers);

const createAppStore = () => {
  let store = createStore(persistedReducer, { items: [] }, storeEnhancer);
  let persistor = persistStore(store);

  return { store, persistor };
};

export default createAppStore;
