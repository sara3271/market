import { combineReducers } from 'redux';
import { itemReducer } from './itemReducer';

const allReducers = combineReducers({
  items: itemReducer,
});
export default allReducers;
