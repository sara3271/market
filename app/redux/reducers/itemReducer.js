import { GET_ITEMS } from '@actions/actionTypes';

export function itemReducer(state = [], { type, payload }) {
  switch (type) {
    case GET_ITEMS:
      return payload.items;
  }

  return state;
}
