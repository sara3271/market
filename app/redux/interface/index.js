import constants from './constants';
import * as R from 'ramda';

const itemsDataType = {
  type: 'type',
};

const bannerItemDataType = {
  id: 'id',
  image: 'image',
  title: 'title',
};

const productItemDataType = {
  id: 'id',
  title: 'title',
  image: 'image',
  price: 'price',
  discount: 'discount',
  discountRatio: 'discountRatio',
};

export function itemInterface(data) {
  const converted = convertor(data, itemsDataType);
  converted['banners'] = [];
  converted['products'] = [];
  converted['image'] = [];
  switch (converted.type) {
    case constants.SLIDE_SWIPE:
      for (let bannerItem of data.banners) {
        converted['banners'].push(convertor(bannerItem, bannerItemDataType));
      }
      break;
    case constants.CAMPAIGN_BANNER:
      for (let bannerItem of data.banners) {
        converted['banners'].push(convertor(bannerItem, bannerItemDataType));
      }
      break;
    case constants.CATEGORY_LIST_VERTICAL:
      for (let bannerItem of data.banners) {
        converted['banners'].push(convertor(bannerItem, bannerItemDataType));
      }
      converted['title'] = data.title;
      break;
    case constants.TIMER_HORIZONTAL:
      converted['title'] = data.title;
      for (let productItem of data.products) {
        converted['products'].push(convertor(productItem, productItemDataType));
        converted['image'].push(
          R.pathOr('', ['images', '0', 'imageSrc'], productItem),
        );
      }
      break;
  }
  return converted;
}

function convertor(data, dataType) {
  const convertedData = {};
  for (let key of Object.keys(data)) {
    const convertedKey = Object.keys(dataType).find(
      item => dataType[item] == key,
    );
    if (!!convertedKey) {
      convertedData[convertedKey] = data[key];
    }
  }
  return convertedData;
}
