import React, { Component } from 'react';
import {
  View,
  Image,
  TouchableOpacity,
  Text,
  TextInput,
  Dimensions,
  StyleSheet,
  FlatList,
  StatusBar,
  ScrollView,
} from 'react-native';

import Colors from '../constants/colors';

const { width, height } = Dimensions.get('window');

const BigBannerItem = ({ banner }) => {
  return (
    <View
      style={{
        justifyContent: 'center',
        alignItems: 'center',
        margin: 12,
      }}
    >
      <Image
        style={{
          width: width / 1.1,
          height: width / 2.2,
          resizeMode: 'cover',
          borderRadius: 5,
        }}
        source={{ uri: `${banner.image}` }}
      />
    </View>
  );
};

const BigBanner = ({ banners }) => {
  return (
    <View style={{ alignItems: 'center', justifyContent: 'center' }}>
      <FlatList
        data={banners}
        renderItem={({ item }) => <BigBannerItem banner={item} />}
        showsHorizontalScrollIndicator={false}
        horizontal={true}
      />
    </View>
  );
};

export default BigBanner;
