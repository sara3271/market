import React, { Component } from 'react';
import { View, ScrollView, TouchableOpacity, Image, Text } from 'react-native';

import Colors from '../constants/colors';
import images from '@assets/images';

const DrawerItem = ({ icon, title }) => (
  <TouchableOpacity style={{ marginVertical: 8, flexDirection: 'row' }}>
    <Image
      style={{
        tintColor: 'white',
        height: 36,
        width: 36,
        resizeMode: 'contain',
      }}
      source={icon}
    />
    <Text style={{ color: 'white', fontSize: 18, marginStart: 16 }}>
      {title}
    </Text>
  </TouchableOpacity>
);

export default class DrawerMenu extends Component {
  render() {
    return (
      <ScrollView
        style={{ flex: 1, backgroundColor: Colors.drawerBackgroundColor }}
      >
        <View style={{ flex: 1, paddingVertical: 64, paddingStart: 32 }}>
          <Text style={{ fontSize: 24, color: 'white' }}>سارا رحیمی</Text>
          <Text style={{ fontSize: 18, color: 'white' }}>۱۵۰۰۰ تومان</Text>
          <View style={{ flex: 1, marginTop: 16 }}>
            <DrawerItem title="سفارش‌های پیشین" icon={images.history} />
            <DrawerItem title="دسته‌بندی‌ها" icon={images.catagory} />
            <DrawerItem title="انتخاب آدرس" icon={images.address} />
            <DrawerItem title="افزایش اعتبار" icon={images.deposit} />
            <DrawerItem title="پیام‌ها" icon={images.messages} />
            <DrawerItem title="پشتیبانی" icon={images.support} />
            <DrawerItem title="خروج" icon={images.exit} />
          </View>
        </View>
      </ScrollView>
    );
  }
}
