import React, { Component } from 'react';
import {
  View,
  Image,
  TouchableOpacity,
  Text,
  TextInput,
  Dimensions,
  StyleSheet,
  FlatList,
  StatusBar,
  ScrollView,
} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialIcons';

import Colors from '../constants/colors';

const { width, height } = Dimensions.get('window');

const VerticalCategoryItem = ({ product }) => {
  return (
    <View style={{ margin: 8, justifyContent: 'center', alignItems: 'center' }}>
      <Image
        style={{
          width: width / 3.8,
          height: width / 3.8,
          borderRadius: 5,
          resizeMode: 'cover',
        }}
        source={{ uri: `${product.image}` }}
      />
      <Text style={{ margin: 4, fontSize: 12 }}> {product.title}</Text>
    </View>
  );
};

const VerticalCategory = ({ products }) => {
  let productsWithImage = products.slice(1, products.length);
  return (
    <View
      style={{ width: width, justifyContent: 'center', alignItems: 'center' }}
    >
      <Text style={{ margin: 8, fontSize: 16 }}>{products[0].title} </Text>
      <FlatList
        data={productsWithImage}
        renderItem={({ item }) => <VerticalCategoryItem product={item} />}
        numColumns={3}
        keyExtractor={(item, index) => index}
      />
    </View>
  );
};

export default VerticalCategory;
