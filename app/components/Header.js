import React, { Component } from 'react';
import {
  View,
  Image,
  TouchableOpacity,
  Text,
  TextInput,
  Dimensions,
  StyleSheet,
  FlatList,
  StatusBar,
  ScrollView,
} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialIcons';

import Colors from '../constants/colors';
import { USER_ADDRESS } from '../constants/constants';

const { width, height } = Dimensions.get('window');

const Header = ({ navigation }) => {
  return (
    <View
      style={{
        padding: 12,
        backgroundColor: Colors.headerColor,
      }}
    >
      <View style={{ flexDirection: 'row', alignItems: 'center' }}>
        <View style={{ marginEnd: 12 }}>
          <Icon name="menu" size={30} />
        </View>
        <View>
          <Text
            style={{
              fontSize: 13,
              marginBottom: 4,
              color: Colors.topTextColor,
            }}
          >
            آدرس شما
          </Text>
          <Text
            style={{
              fontSize: 15,
              color: Colors.bottomTextColor,
              width,
            }}
          >
            {USER_ADDRESS}
          </Text>
        </View>
      </View>
      <View style={{ paddingTop: 20, paddingHorizontal: 8 }}>
        <TextInput
          style={{
            backgroundColor: Colors.white,
            height: height / 15,
            borderRadius: height / 30,
            borderWidth: 1,
            borderColor: Colors.searchInputBorder,
            paddingHorizontal: 16,
            fontFamily: 'IRANSansMobileMedium',
            fontSize: 12,
          }}
          placeholder={'به دنبال چه می‌گردید؟'}
        />
      </View>
    </View>
  );
};

export default Header;
