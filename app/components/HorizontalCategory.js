import React, { Component } from 'react';
import {
  View,
  Image,
  TouchableOpacity,
  Text,
  TextInput,
  Dimensions,
  StyleSheet,
  FlatList,
  StatusBar,
  ScrollView,
} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialIcons';

import Colors from '../constants/colors';

const { width, height } = Dimensions.get('window');

const HorizontalCategoryItem = ({ product, image }) => {
  return (
    <View
      style={{
        flex: 1,
        marginHorizontal: 12,
      }}
    >
      <View>
        <Image
          style={{
            ...StyleSheet.absoluteFill,
            width: width / 3.5,
            height: height / 5.6,
            resizeMode: 'contain',
          }}
          source={{ uri: `${image}` }}
        />
        <View
          style={{
            width: width / 3.5,
            height: height / 5.6,
            justifyContent: 'space-between',
          }}
        >
          <Icon name="add-circle" size={25} color={Colors.lightGreen} />
          <View
            style={{
              backgroundColor: Colors.lightGreen,
              width: width / 6,
              borderRadius: 5,
            }}
          >
            <Text
              style={{
                textAlign: 'center',
                color: 'white',
                fontSize: 12,
              }}
            >
              {product.discountRatio}% تخفیف
            </Text>
          </View>
        </View>
      </View>
      <View
        style={{ flexDirection: 'row', marginTop: 12, alignItems: 'center' }}
      >
        <Text style={{ fontSize: 14, marginEnd: 4 }}>
          {product.price} تومان
        </Text>
        <Text
          style={{
            textDecorationLine: 'line-through',
            fontSize: 12,
          }}
        >
          {product.price + product.discount}
        </Text>
      </View>
      <Text
        style={{
          marginTop: 12,
          width: 100,
          fontSize: 12,
        }}
      >
        {product.title}
      </Text>
    </View>
  );
};

const HorizontalCategory = ({ products, images }) => {
  return (
    <View style={{ backgroundColor: Colors.white, paddingVertical: 12 }}>
      <FlatList
        data={products}
        renderItem={({ item, index }) => {
          return (
            <HorizontalCategoryItem product={item} image={images[index]} />
          );
        }}
        showsHorizontalScrollIndicator={false}
        horizontal={true}
      />
    </View>
  );
};

export default HorizontalCategory;
