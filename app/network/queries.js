import { SERVER_URL } from '../constants/constants';

export async function getProductsQuery() {
  const options = {
    method: 'GET',
  };
  const response = await fetch(SERVER_URL, {
    ...options,
  });
  const responseJson = await response.json();
  if (response && response.ok) {
    return responseJson.data.menus;
  } else {
    return responseJson.message;
  }
}
