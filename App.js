import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  I18nManager,
  Dimensions,
  AsyncStorage,
} from 'react-native';
import { createBottomTabNavigator, DrawerNavigator } from 'react-navigation';
import Orientation from 'react-native-orientation-locker';
import Icon from 'react-native-vector-icons/MaterialIcons';
import { Provider } from 'react-redux';
import { PersistGate } from 'redux-persist/integration/react';
import { setCustomTextInput, setCustomText } from 'react-native-global-props';
import RNRestart from 'react-native-restart';

import * as screens from '@screens';
import Colors from './app/constants/colors';
import createAppStore from './app/redux/store';
import * as components from '@components';

const { store, persistor } = createAppStore();
const { width, height } = Dimensions.get('window');

const screenObject = (persianName, englishName) => {
  return {
    tabBarLabel: persianName,
    tabBarIcon: ({ tintColor }) => (
      <Icon name={englishName} size={30} color={tintColor} />
    ),
  };
};

const MainDrawerNavigator = DrawerNavigator(
  {
    Main: { screen: screens.HomePage },
  },
  {
    drawerPosition: 'right',
    drawerOpenRoute: 'DrawerOpen',
    drawerCloseRoute: 'DrawerClose',
    drawerBackgroundColor: '#c7c7c7',
    drawerWidth: width,
    contentComponent: components.DrawerMenu,
    header: {
      visible: false,
    },
  },
);

const MainTabNavigator = createBottomTabNavigator(
  {
    HomePage: {
      screen: MainDrawerNavigator,
      navigationOptions: screenObject('خانه ', 'home'),
    },
    ShopList: {
      screen: screens.ShopList,
      navigationOptions: screenObject('لیست خرید', 'toc'),
    },
    SearchPage: {
      screen: screens.SearchPage,
      navigationOptions: screenObject('جستجو', 'search'),
    },
    Categories: {
      screen: screens.Categories,
      navigationOptions: screenObject('دسته‌بندی', 'dashboard'),
    },
    CartPage: {
      screen: screens.CartPage,
      navigationOptions: screenObject('سبد خرید', 'shopping-cart'),
    },
  },
  {
    order: ['HomePage', 'ShopList', 'SearchPage', 'Categories', 'CartPage'],
    tabBarOptions: {
      activeTintColor: Colors.tabIconColor,
      inactiveTintColor: Colors.gray,
      style: {
        backgroundColor: Colors.white,
      },
    },
  },
);

export default class MarketApp extends Component {
  async componentDidMount() {
    Orientation.lockToPortrait();
    I18nManager.forceRTL(true);
    const RTL_KEY = '@SnappMarket:isRTL';
    const value = await AsyncStorage.getItem(RTL_KEY);
    if (value !== 'true') {
      RNRestart.Restart();
      await AsyncStorage.setItem(RTL_KEY, 'true');
    }

    this.setCustomProps();
  }

  setCustomProps() {
    const customTextProps = {
      style: {
        fontFamily: 'IRANSansMobileMedium',
      },
    };
    setCustomTextInput(customTextProps);
    setCustomText(customTextProps);
  }

  render() {
    return (
      <Provider store={store}>
        <PersistGate loading={null} persistor={persistor}>
          <MainTabNavigator />
        </PersistGate>
      </Provider>
    );
  }
}
