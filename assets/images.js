const images = {
  address: require('./images/address.png'),
  catagory: require('./images/catagory.png'),
  deposit: require('./images/deposit.png'),
  exit: require('./images/exit.png'),
  history: require('./images/history.png'),
  messages: require('./images/messages.png'),
  support: require('./images/support.png'),
};

export default images;
